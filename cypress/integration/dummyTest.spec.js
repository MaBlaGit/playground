/// <reference types="Cypress" />

describe('Lambda101Cypress', () => {
  it('Vist LambdaTest page', () => {
    cy.visit('https://www.lambdatest.com/');
    cy.get(
      'img[src*="https://www.lambdatest.com/resources/images/logos/logo.svg"]'
    ).should('exist');
  });

  it.only('Muse events', () => {
    cy.visit('https://opensource-demo.orangehrmlive.com/index.php/auth/login');
    cy.get('#txtUsername').type('Admin');
    cy.get('#txtPassword').type('admin123');
    cy.get('#btnLogin').click();
    cy.get('#welcome').should('have.text', 'Welcome Paul');
    cy.get('#menu_admin_viewAdminModule')
      .should('be.visible')
      .trigger('mouseover');
    cy.wait(10000);
  });
});
